from flask import Flask, request, render_template
import openpyxl
from openpyxl import Workbook
from flask import Flask, render_template

app = Flask(__name__, template_folder='templates')


# Use an in-memory workbook for simplicity. You can use a file path if needed.
workbook = Workbook()
sheet = workbook.active
sheet.append(["Name", "Email", "Age"])

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/submit", methods=["POST"])
def submit():
    name = request.form.get("name")
    email = request.form.get("email")
    age = request.form.get("age")

    # Append data to the sheet
    sheet.append([name, email, age])

    # Save the workbook
    workbook.save("data.xlsx")

    return "Data submitted successfully!"

if __name__ == "__main__":
    app.run(debug=True)
